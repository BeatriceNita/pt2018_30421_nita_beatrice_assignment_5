package lambdaExpAndStreams;

import java.util.Date;

public class MonitoredData {

	private Date start_time;
	private Date end_time;
	private String activityLabel;
	
	public MonitoredData() {
		//this.start_time = start_time;
		//this.end_time =end_time;
		//this.activityLabel = activityLabel;
	}
	
	public Date getStartTime() {
		return start_time;
	}

	public void setStartTime(Date startTime) {
		this.start_time = startTime;
	}

	public Date getEndTime() {
		return end_time;
	}

	public void setEndTime(Date endTime) {
		this.end_time = endTime;
	}

	public String getActivityLabel() {
		return activityLabel;
	}

	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}

	public String toString() {
		return String.format("MonitoredData -> start time: " + getStartTime().toString() + ", end time: " + getEndTime().toString() + ", activity: " + getActivityLabel().toString());
		
	}
}
