package lambdaExpAndStreams;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import lambdaExpAndStreams.MonitoredData;

public class Main {

	static DateFormat date = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
	
	public static List<MonitoredData> readingFromFile() {
		String fileName = "src/Activities.txt";
		
		List<MonitoredData> monitoredData = new ArrayList<MonitoredData>();
		
		try 
		{	monitoredData =  Files.lines(Paths.get(fileName)).map(line -> 
								{
									MonitoredData data = new MonitoredData();
									String[] fields = line.trim().split("\\t+");
						
									String startTimePart = fields[0];
									Date startTimeDate = null;
									
										try {
											startTimeDate = date.parse(startTimePart);
										} catch (ParseException e) {
											e.printStackTrace();
										}
									
									String endTimePart = fields[1];
									Date endTimeDate = null;
									
										try {
											endTimeDate = date.parse(endTimePart);
										} catch (ParseException e) {
											e.printStackTrace();
										}
									
									String activityPart = fields[2];
									
										data.setStartTime(startTimeDate);
										data.setEndTime(endTimeDate);
										data.setActivityLabel(activityPart);
									
									return data;
								}).collect(Collectors.toList());
		} catch (IOException e) 
		{
			e.printStackTrace();
		}
		return monitoredData;
	}
	
	public static <T> Predicate<T> distinctByDay(Function<? super T, Object> keyExtractor){
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

	@SuppressWarnings("deprecation")
	public static long distinctDaysCount(List<MonitoredData> monitoredData) {
		long nrOfDistinctDays = 0;
		
		nrOfDistinctDays = monitoredData.stream().filter(distinctByDay( m -> ((MonitoredData) m).getStartTime().getDate())).count();
		
		System.out.println("The number of distinct days that appear in the monitored data is: " + nrOfDistinctDays);
		return nrOfDistinctDays;
	}
	
	public static Map<String, Integer> distinctActionOccurrences(List<MonitoredData> monitoredData){
		
		Map<String,Integer> map = monitoredData.stream().collect(Collectors.toMap(m -> m.getActivityLabel(), m -> 1, Integer::sum));
		
		String path = new File("").getAbsolutePath();
		try {
			FileWriter distinctActivitiesFile = new FileWriter(path + "\\MapWithDistinctActivities.txt");
			BufferedWriter writer = new BufferedWriter(distinctActivitiesFile);
				for(Map.Entry<String, Integer> entry : map.entrySet()) {
					writer.newLine();
					writer.write("Activity -> " + entry.getKey() + " has number of occurrences equal to " + entry.getValue());
					writer.newLine();
				}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}
	
	@SuppressWarnings("deprecation")
	public static Map<Integer,Map<String,Integer>> dailyActivityCount(List<MonitoredData> monitoredData){
		Map<Integer,Map<String,Integer>> map = monitoredData.stream().collect(
											Collectors.groupingBy(m -> m.getStartTime().getDate(),
											Collectors.groupingBy(m -> m.getActivityLabel(),Collectors.summingInt(m->1)))
											);
										
		String path = new File("").getAbsolutePath();
		try {
			FileWriter dailyActivityCount = new FileWriter(path + "\\DailyActivityCount.txt");
			BufferedWriter writer = new BufferedWriter(dailyActivityCount);
				for(Entry<Integer, Map<String, Integer>> entry : map.entrySet()) {
					writer.newLine();
					writer.write("Day " + entry.getKey().toString() + " => " + entry.getValue());
					writer.newLine();
				}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}
	
	public static void main(String[] args) {
		List<MonitoredData> myList = readingFromFile();

		distinctDaysCount(myList);
		distinctActionOccurrences(myList);
		dailyActivityCount(myList);

	}

}